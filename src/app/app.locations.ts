// tslint:disable: trailing-comma
export const THIS_APP_LOCATIONS = [
  {
    privilege: 'Location',
    target: {
        url: '^/auth/'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    target: {
        url: '^/error'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningUsers'
    },
    target: {
        url: '^/requests'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningUsers'
    },
    target: {
        url: '^/dining/requests/*'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningUsers'
    },
    target: {
        url: '^/dining/cards/*'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningUsers'
    },
    target: {
        url: '^/dining/messages/*'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningUsers'
    },
    target: {
        url: '^/dining/dining-preference/*'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'DiningAdministrators'
    },
    target: {
        url: '^/'
    },
    mask: 1
  },
  {
    privilege: 'Location',
    account: {
        name: 'Administrators'
    },
    target: {
        url: '^/'
    },
    mask: 1
  }
];
