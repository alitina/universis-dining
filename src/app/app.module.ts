import { CommonModule, HashLocationStrategy, LocationStrategy, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, Title } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppSidebarModule } from '@coreui/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { AngularDataContext, DATA_CONTEXT_CONFIG } from '@themost/angular';
import { APP_LOCATIONS, AuthModule, ConfigurationService, ErrorModule,
  LocalUserStorageService, SharedModule, SIDEBAR_LOCATIONS, UserStorageService } from '@universis/common';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ModalModule } from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexComponent } from './layouts/index.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// LOCALES: import extra locales here
import el from '@angular/common/locales/el';
import en from '@angular/common/locales/en';

import { THIS_APP_LOCATIONS } from './app.locations';
import { APP_SIDEBAR_LOCATIONS } from './app.sidebar.locations';
import { AdvancedFormsModule } from '@universis/forms';
import { AdminDiningModule } from '@universis/ngx-dining/admin';
import { DiningSharedModule } from '@universis/ngx-dining/shared';
import { ProgressbarModule } from 'ngx-bootstrap/progressbar';
import { NgxSignerModule } from '@universis/ngx-signer';
import { DINING_ADMIN_LOCALES } from 'src/assets/i18n';

@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot(),
    SharedModule.forRoot(),
    RouterModule,
    AuthModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    AppSidebarModule,
    ErrorModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot(),
    AdvancedFormsModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    AdminDiningModule,
    DiningSharedModule.forRoot(),
    ProgressbarModule.forRoot(), 
    NgxSignerModule.forRoot()
  ],
  providers: [
    Title,
    {
        provide: DATA_CONTEXT_CONFIG, useValue: {
            base: '/',
            options: {
                useMediaTypeExtensions: false,
                useResponseConversion: true,
            },
        },
    },
    {
        provide: APP_LOCATIONS,
        useValue: THIS_APP_LOCATIONS,
    },
    {
      provide: SIDEBAR_LOCATIONS,
      useValue: APP_SIDEBAR_LOCATIONS,
    },
    AngularDataContext,
    {
        provide: APP_INITIALIZER,
        // use APP_INITIALIZER to load application configuration
        useFactory: (configurationService: ConfigurationService) =>
            () => {
            // load application configuration
                return configurationService.load().then(() => {
                    // LOCALES: register application locales here
                    registerLocaleData(en);
                    registerLocaleData(el);
                    // return true for APP_INITIALIZER
                    return Promise.resolve(true);
                });
            },
        deps: [ ConfigurationService ],
        multi: true,
    },
    {
        provide: LOCALE_ID,
        useFactory: (configurationService: ConfigurationService) => {
            return configurationService.currentLocale;
        },
        deps: [ConfigurationService],
    },
    // use hash location stategy
    // https://angular.io/api/common/HashLocationStrategy
    {
        provide: LocationStrategy,
        useClass: HashLocationStrategy,
    },
    {
      provide: UserStorageService,
      useClass: LocalUserStorageService,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  // tslint:disable-next-line: variable-name
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach( language => {
      if (DINING_ADMIN_LOCALES.hasOwnProperty(language)) {
        this._translateService.setTranslation(language, DINING_ADMIN_LOCALES[language], true);
      }
    });
  }

}
