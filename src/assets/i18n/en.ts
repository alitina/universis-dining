export const en = {
  "Sidebar": {
    "Messages": "Messages",
    "DiningCards":"Dining Cards", 
    "DiningRequests": "Dining Requests",
    "DiningPreference": "Dining Preference",
    "More": "More",
    "DiningAdmin": "Settings"
  },
  "Logout": "Logout",
  "Profile": "My profile",
  "Languages": {
    "en": "English",
    "el": "Ελληνικά"
  },
  "Cancel": "Cancel"
}
