export const el = {
  "Sidebar": {
    "Messages": "Μηνύματα",
    "More": "Περισσότερα",
    "Settings": "Ρυθμίσεις",
    "DiningCards": "Δικαιώματα Σίτισης",
    "DiningRequests": "Αιτήσεις Σίτισης",
    "DiningPreference": "Πρόθεση σίτισης",
    "DiningAdmin": "Ρυθμίσεις"
  },
    "Logout": "Αποσύνδεση",
    "Profile": "Το προφίλ μου",
    "Languages": {
      "en": "English",
      "el": "Ελληνικά"
    },
    "Cancel": "Άκυρο"
  }
  